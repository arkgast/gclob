const fs = require('fs')
const { Datastore } = require('@google-cloud/datastore')

const datastore = new Datastore()
const writeFileStream = fs.createWriteStream('signers.csv')

const query = datastore.createQuery('Signer').select('handle')

datastore
  .runQueryStream(query)
  .on('data', ({ handle }) => {
    writeFileStream.write(`${handle}\n`)
  })
  .on('end', () => {
    console.log('complete reading!')
  })
