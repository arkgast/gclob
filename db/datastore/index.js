const fs = require('fs')
const { Datastore } = require('@google-cloud/datastore')

const datastore = new Datastore()
const writeFileStream = fs.createWriteStream('ds-ious.csv')

const writeLine = (hash, created) => {
  const line = `${hash},${created}\n`
  writeFileStream.write(line)
}

const query = datastore.createQuery('Transaction')

datastore
  .runQueryStream(query)
  .on('data', data => {
    writeLine(data.iou.hash.value, data.created)
  })
  .on('end', () => {
    console.log('complete reading!')
  })
