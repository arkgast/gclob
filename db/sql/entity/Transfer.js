module.exports = {
  name: 'Transfer',
  columns: {
    transferId: {
      primary: true,
      unique: true,
      type: 'varchar',
      length: 36
    },
    type: {
      type: 'varchar',
      length: 16,
      nullable: true
    },
    status: {
      type: 'varchar',
      length: 16,
      nullable: true
    },
    created: {
      type: 'varchar',
      length: 32,
      nullable: true
    },
    updated: {
      type: 'varchar',
      length: 32,
      nullable: true
    },
    labels: {
      type: 'json'
    },
    snapshot: {
      type: 'json'
    }
  }
}
