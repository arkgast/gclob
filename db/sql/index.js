const fs = require('fs')
const { createConnection, getRepository } = require('typeorm')
const config = require('./typeorm-config')

const writeFileStream = fs.createWriteStream('sql-ious.csv')

const writeLine = ({ hash, created, type, status, transferId }) => {
  const line = `${hash},${created},${type},${status},${transferId}\n`
  writeFileStream.write(line)
}

const getLabelsData = data => {
  try {
    const labels = JSON.parse(data.labels)
    return {
      hash: labels.hash,
      created: labels.created,
      type: labels.type,
      status: labels.status,
      transferId: labels.tx_ref
    }
  } catch {
    return {}
  }
}

createConnection(config).then(() => {
  getRepository('Action')
    .createQueryBuilder()
    .select('labels')
    .stream()
    .then(streaming => {
      streaming
        .on('data', data => {
          const labels = getLabelsData(data)
          if (labels.hash !== 'PENDING') {
            writeLine(labels)
          }
        })
        .on('end', () => {
          console.log('complete reading!')
        })
    })
})
