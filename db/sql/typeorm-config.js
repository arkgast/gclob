require('dotenv').config()
const typeorm = require('typeorm')
const EntitySchema = typeorm.EntitySchema

const ActionEntity = require('./entity/Action')

const { MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_HOST } = process.env

const connectionConfig = {
  type: 'mysql',
  username: MYSQL_USER,
  password: MYSQL_PASSWORD,
  database: MYSQL_DATABASE,
  host: MYSQL_HOST,
  synchronize: false,
  entities: [new EntitySchema(ActionEntity)]
}

module.exports = connectionConfig
