const Promise = require('bluebird')
const { resolve } = require('path')
const fs = Promise.promisifyAll(require('fs'))

const writeStream = fs.createWriteStream('ds-unprocessed-full.csv')

const dsFullFilePath = resolve(__dirname, '..', 'ious', '_ds-ious.csv')
const dsUnprocessedFilePath = resolve(
  __dirname,
  '..',
  'ious',
  'ds-ious-unprocessed.csv'
)

const readFile = async fileName => {
  const fileContent = await fs.readFileAsync(fileName, 'utf8')
  const lines = fileContent.split('\n')
  lines.pop()
  return lines
}

const getFilesContent = async () => {
  return Promise.all([
    readFile(dsFullFilePath),
    readFile(dsUnprocessedFilePath)
  ])
}

const main = async () => {
  const [dsFull, dsUncompressed] = await getFilesContent()

  for (const iou of dsUncompressed) {
    for (const iouData of dsFull) {
      if (iouData.indexOf(iou) === 0) {
        writeStream.write(`${iouData}\n`)
        break
      }
    }
  }
}

main()
