const Promise = require('bluebird')
const { get } = require('./get')
const path = require('path')
const fs = Promise.promisifyAll(require('fs'))

const ERROR = {
  NOT_PROCESSED: 0,
  FATAL: 1
}

const fileName = 'sql-ious'
const writeStreamProcessed = fs.createWriteStream(`${fileName}-processed.csv`)
const writeStreamUnprocessed = fs.createWriteStream(
  `${fileName}-unprocessed.csv`
)
const writeStreamError = fs.createWriteStream(`${fileName}-error.csv`)

const getLines = fileContent => {
  const lines = fileContent.split('\n')
  lines.pop()
  return lines
}

const getIOUData = async iouHash => {
  const { data } = await get(`iou/${iouHash}`).catch(err => {
    const { status } = err.response
    const error = status === 404 ? ERROR.NOT_PROCESSED : ERROR.FATAL
    return { data: { iou: iouHash, transaction: error } }
  })
  return data
}

const writeLine = ({ iou, transaction }) => {
  if (transaction === ERROR.NOT_PROCESSED) {
    writeStreamUnprocessed.write(`${iou}\n`)
  } else if (transaction === ERROR.FATAL) {
    writeStreamError.write(`${iou}\n`)
  } else {
    writeStreamProcessed.write(`${iou},${transaction}\n`)
  }
}

fs.readFileAsync(
  path.resolve(__dirname, '..', 'ious', `${fileName}.csv`),
  'utf8'
)
  .then(data => {
    Promise.map(
      getLines(data),
      iouHash => {
        return getIOUData(iouHash).then(iouData => {
          writeLine(iouData)
        })
      },
      { concurrency: 50 }
    ).then(() => {
      console.log('done!')
    })
  })
  .catch(error => {
    console.error(error)
  })
