const Promise = require('bluebird')
const path = require('path')
const fs = Promise.promisifyAll(require('fs'))
const { get } = require('./get')

const fileName = 'signers'
const writeStream = fs.createWriteStream(`${fileName}-balance.csv`)
const writeStreamError = fs.createWriteStream(`${fileName}-error.csv`)

const getLines = fileContent => {
  const lines = fileContent.split('\n')
  lines.pop()
  return lines
}

const writeLine = (signer, balance) => {
  if (balance > 0) {
    writeStream.write(`${signer},${balance}\n`)
  }
}

const getBalance = async signer => {
  const balance = await get(`api/signers/${signer}/balance`)
  return balance
}

fs.readFileAsync(path.resolve(__dirname, '..', `${fileName}.csv`), 'utf8')
  .then(data => {
    let idx = 0
    Promise.map(
      getLines(data),
      signer => {
        return getBalance(signer)
          .then(balance => {
            console.log(++idx, signer, balance)
            writeLine(signer, balance)
            return balance
          })
          .catch(() => {
            console.log(++idx, signer)
            writeStreamError.write(`${signer}\n`)
            return -1
          })
      },
      { concurrency: 50 }
    ).then(() => {
      console.log('done!')
    })
  })
  .catch(error => {
    console.error(error)
  })
