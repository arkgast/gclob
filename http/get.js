const axios = require('axios')

const URL_BASE = 'http://localhost:3001/'

const get = async url => {
  const { data } = await axios.get(URL_BASE + url)
  return data
}

module.exports = { get }
