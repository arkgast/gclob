const Promise = require('bluebird')
const path = require('path')
const fs = Promise.promisifyAll(require('fs'))
const { Datastore } = require('@google-cloud/datastore')

const datastore = new Datastore()
// Name of the file that contains the action_ids to update
const fileName = 'actions'

const writeErrorStream = fs.createWriteStream(`${fileName}-error.csv`)
const writeSuccessStream = fs.createWriteStream(`${fileName}-success.csv`)

const getLines = fileContent => {
  const lines = fileContent.split('\n')
  return lines
}

const updateStatus = actionId => {
  if (!actionId) return
  console.log({ actionId })

  return new Promise((resolve, reject) => {
    const key = datastore.key(['Action', actionId])

    datastore.get(key, (err, entity) => {
      if (err) {
        writeErrorStream.write(`${actionId},READ,${JSON.stringify(err)}\n`)
        reject(err)
      }

      const updatedEntity = {
        key,
        data: {
          ...entity,
          labels: {
            ...entity.labels,
            status: 'PENDING'
          },
          error: {
            code: '0',
            message: 'Success'
          }
        }
      }

      datastore.update(updatedEntity, err => {
        if (err) {
          writeErrorStream.write(`${actionId},UPDATE,${JSON.stringify(err)}\n`)
          reject(err)
        }
        writeSuccessStream.write(`${actionId}\n`)
        resolve(actionId)
      })
    })
  })
}

fs.readFileAsync(path.resolve(__dirname, '..', `${fileName}.csv`), 'utf8')
  .then(data => {
    Promise.map(
      getLines(data),
      actionId => {
        return updateStatus(actionId)
      },
      { concurrency: 30 }
    ).then(() => {
      console.log('done!')
    })
  })
  .catch(error => {
    console.error(error)
  })
